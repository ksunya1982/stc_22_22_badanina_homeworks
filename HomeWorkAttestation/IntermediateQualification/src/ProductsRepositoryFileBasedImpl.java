import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {
    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer getId = Integer.parseInt(parts[0]);
        String getName = parts[1];
        Double getCost = Double.parseDouble(parts[2]);
        Integer getCount = Integer.parseInt(parts[3]);
        return new Product(getId, getName, getCost, getCount);
    };
    private static final Function<Product, String> productToStringMapper = product -> {
        return product.getId().toString() + "|" + product.getName() + "|" + product.getPrice().toString() + "|" + product.getCount().toString();
    };
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Product findById(Integer id) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(it -> it.getId().equals(id))
                    .findFirst()
                    .orElseGet(() -> new Product(0, "", 0.0, 0));
        } catch (IOException ex) {
            throw new IOException();
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(it -> it.getName().toLowerCase().contains(title.toLowerCase()))
                    .toList();
        } catch (IOException ex) {
            throw new IOException();
        }
    }

    @Override
    public void update(Product product) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName));
             FileWriter writer = new FileWriter(fileName, false)) {
            // 1. Считали из файла
            List<Product> productList = reader.lines().map(stringToProductMapper).toList();
            // 2. Считали объект
            Product oldProduct = productList.stream().filter(it -> it.getId().equals(product.getId())).findFirst().get();
            // 3. Обновили
            Product newProduct = new Product(oldProduct.getId(), oldProduct.getName(), oldProduct.getPrice(), oldProduct.getCount());
            // 4. Записали в файл
            List<Product> products = productList.stream().map(it -> {
//            productList.stream().map(it -> {
//                if (it.GetId() == newProduct.GetId()) {
                if (Objects.equals(it.getId(), newProduct.getId())) {
                    return newProduct;
                }
                return it;
            }).toList();
            // 5. Сохранили
            saveAll(products);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void saveString(Product product) throws IOException {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String productToSave = productToStringMapper.apply(product);
            bufferedWriter.write(productToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    public void saveAll(List<Product> products) throws IOException {
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            StringBuilder stringProduct = new StringBuilder();
            for (Product product : products) {
                stringProduct.append(productToStringMapper.apply(product)).append("\n");
            }
            bufferedWriter.write(stringProduct.toString());
//            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IOException(e);
        }
    }
}
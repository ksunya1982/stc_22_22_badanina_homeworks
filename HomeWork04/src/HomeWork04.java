import java.util.Scanner;

public class HomeWork04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("From:");
        int from = scanner.nextInt();
        System.out.println("To:");
        int to = scanner.nextInt();
        System.out.println("Array size:");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for(int i= 0; i < size; i++) {
            System.out.println("Array {" + i + "}:");
            array[i] = scanner.nextInt();
        }
        System.out.println(sum(from, to, array));
        even(array);

    }
    public static int sum(int from, int to, int[] array) {
        int sum = 0;
        if (from > to || from < 0 || to >= array.length) {
            return -1;
        }
        for (int i = from; i <= to; i++) {
            sum += array[i];
        }
        return  sum;
    }
    public static void even(int[] array) {
        for (int j : array) {
            if (j % 2 == 0) {
                System.out.println(j);
            }
        }
    }

}

